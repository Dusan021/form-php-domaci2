<!DOCTYPE HTML>
<html>
<head>
</head>
<body>

<?php
// define variables and set to empty values
$nameErr = $lnameErr = $emailErr = $genderErr = $websiteErr = $grodjenjaErr = "";
$name = $lname = $email = $gender = $comment = $website = $grodjenja =  "";
$chckErr = false;
//$provera = false;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Name
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  }

  else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed";
    }
  }
  if (strlen($name) < 2) {
    $nameErr = "min 2 letters";
  }
    //Last name

  if (empty($_POST["lname"])) {
      $lnameErr = "Last name is required";
    }
    else {
      $lname = test_input($_POST["lname"]);
      // check if name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
        $lnameErr = "Only letters and white space allowed";
      }
    }
    if (strlen($lname) < 2) {
      $lnameErr = "min 2 letters";
    }

  if (empty($_POST["email"])) {
      $emailErr = "Email is required";
    }

    //email

  else {
    $email = test_input($_POST["email"]);
    $sjec = explode("@", $email);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    }
    else if(checkEmailAdress($email))
    {
      $emailErr = "You need gmail.com acc";
    }
    else if (strlen($sjec[0])<6)
    {
      $emailErr = "Minimum 6 letters";
    }
  }

  //website

  if (empty($_POST["website"])) {
      $website = "";
    }

  else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL";
    }
  }
  //comment

  if (empty($_POST["comment"])) {
    $comment = "";
  }

  else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  }

  else {
    $gender = test_input($_POST["gender"]);
  }

  //year of birth
  if (empty($_POST["grodjenja"])) {
    $grodjenjaErr = "Year of birth is required";
  }

  else {
    $grodjenja = test_input($_POST["grodjenja"]);
  }

  if (!empty($nameErr) or !empty($lnameErr) or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr) or !empty($grodjenjaErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&lname=" . urlencode($_POST["lname"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);
    $params .= "&grodjenja=" . urlencode($_POST["grodjenja"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lnameErr=" . urlencode($lnameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
    $params .= "&grodjenjaErr=" . urlencode($grodjenjaErr);

    header("Location: index.php?" . $params);

  }

  else {
    echo "<h2>Your Input:</h2>";
    echo "Name: " . $_POST['name'];
    echo "<br>";


    echo "Last Name: " . $_POST['lname'];
    echo "<br>";

    echo "Email: " . $_POST['email'];
    echo "<br>";

    echo "Website: " . $_POST['website'];
    echo "<br>";

    echo "Comment: " . $_POST['comment'];
    echo "<br>";

    echo "Gender: " . $_POST['gender'];
    echo "<br>";
    echo "<br>";
    $dob = $_POST['grodjenja'];
    echo "Year of Birth: " .date('l,m,Y',strtotime($dob));
    echo "<br>";
    echo "<br>";
    echo date('l');
    echo "<br>";
    echo "<br>";
    echo "<a href=\"index.php\">Return to form</a>";
  }
}

function checkEmailAdress($email2) {
  $zadnjideomejla = explode("@", $email2);
  if($zadnjideomejla[1] != "gmail.com") {
    //$provera = false
      return true;
    }
  else {
      return false;
  }

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>

</body>
</html>
